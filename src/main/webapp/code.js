$(document).ready(() => {

	const form = $('#employeeForm')[0];
	const modal = $('#employeeModal');

	const employeeData = $('#employeeList').DataTable({
		"serverSide": true,
		"lengthChange": true,
		"processing": false,
		"order":[1],
		"ajax":{
			url: "/api/employees",
			type: "GET"
		},

		"columns": [
			{ "searchable": false, "orderable": false },
			{ "searchable": true },
			{ "searchable": false },
			{ "searchable": false },
			{
				"mData": null,
				"bSortable": false,
				"mRender": (row) => {
					const id = row[0];

					return `<button 
  						type="button" name="update" id="${id}" 
  						class="btn btn-warning update">Update</button>`;
				}
			},
			{
				"mData": null,
				"bSortable": false,
				"mRender": (row) => {
					const id = row[0];

					return `<button type="button" name="delete" id="${id}" 
                            class="btn btn-danger delete">Delete</button>`;
				}
			}
		],

		"pageLength": 10,
		"bInfo": false,
	});		

	$('#addEmployee').click(() => {
		form.reset();
		modal.modal('show');
	});

	$("#employeeModal").on('submit', '#employeeForm', (event) => {
		event.preventDefault();

		const data = readFromForm(form);
		const url = "/api/employees";
		const callback = () => {
			modal.modal('hide');
			form.reset();
			employeeData.ajax.reload();
		};

		if (data.id) {
			new Http().put(url, data).then(callback);
		} else {
			new Http().post(url, data).then(callback);
		}
	});

	const employeeList = $("#employeeList");

	employeeList.on('click', '.update', (event) => {
		const url = '/api/employees?id=' + $(event.target).attr("id");

		new Http().get(url).then(data => {
			writeToForm(form, data);
			modal.modal('show');
		})
	});

	employeeList.on('click', '.delete', (event) => {

		if(!confirm('Are you sure you want to delete this employee?')) {
			return false;
		}

		const url = '/api/employees?id=' + $(event.target).attr('id');

		new Http().delete(url)
			.then(() => employeeData.ajax.reload())
	});

	function writeToForm(form, data) {
		for (const key of Object.keys(data)) {
			$(form[key]).val(data[key]);
		}
	}

	function readFromForm(form) {

		const keys = $(form).find('input').toArray()
			.filter(f => f.type !== 'submit')
			.map(f => f.name);

		const data =  {};

		for (const key of keys) {
			data[key] = $(form[key]).val();
		}

		return data;
	}

});