
class Http {

    get(url) {
        return fetch(url).then(response => response.json());
    }

    put(url, data) {
        return this.submitCommon(url, 'PUT', data);
    }

    post(url, data) {
        return this.submitCommon(url, 'POST', data);
    }

    submitCommon(url, method, data) {
        return fetch(url, {
            method: method,
            headers: { "Content-type": "application/json" },
            body: JSON.stringify(data)
        });
    }

    delete(url) {
        return fetch(url, { method: 'DELETE' });
    }

}
